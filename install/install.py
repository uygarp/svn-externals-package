#! /usr/bin/env python
#
# SEP installation script
# Author: uygarp
#
# Vestel Electronics, 2015
#

import platform
import os
import shutil
import stat

def which(pgm):
    path=os.getenv('PATH')
    for p in path.split(os.path.pathsep):
        p=os.path.join(p,pgm)
        if os.path.exists(p) and os.access(p,os.X_OK):
            return p

def isSystem(system):
    return platform.system() == system

package_name="SEP (SVN Externals Package)"
svn_exec="svn"
svn_actual="svnactual"
from_dir="packages"
svn_script="svn"
svn_jar="svn.jar"
if isSystem('Windows'):
    svn_exec+=".exe"
    svn_actual+=".exe"
    svn_script+=".bat"

if isSystem('Windows') | isSystem('Linux'):
    svn_full_path=which(svn_exec)
    svn_actual_full_path=which(svn_actual)
    
    if svn_actual_full_path is not None:
        print "\n" + package_name + " is already INSTALLED...\n"
    elif svn_full_path is not None:
        
        path, file = os.path.split(svn_full_path)
        path_actual = path + "/" + svn_actual
        os.rename(svn_full_path, path_actual)
        print "\nRenamed actual SVN executable."
        
        path_script = path + "/" + svn_script
        shutil.copy(from_dir + "/" + svn_script, path_script)
        print "Copied SVN script."
        
        path_jar =  path + "/" + svn_jar
        shutil.copy(from_dir + "/" + svn_jar, path_jar)
        print "Copied executable Jar file."
        
        if isSystem('Linux'):
            st = os.stat(path_actual)
            os.chmod(path_actual, st.st_mode | stat.S_IEXEC | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)
            st = os.stat(path_script)
            os.chmod(path_script, st.st_mode | stat.S_IEXEC | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)
            st = os.stat(path_jar)
            os.chmod(path_jar, st.st_mode | stat.S_IEXEC | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)
        
        print "\n" + package_name + " was INSTALLED...\n"
    else:
        print "\nSVN is not installed in this computer.\nPlease, install SVN Command Line Interface...\n\n"

else:
    print 'Unknown Platform'

    
