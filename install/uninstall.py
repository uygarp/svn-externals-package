#! /usr/bin/env python
#
# SEP uninstallation script
# Author: uygarp
#
# Vestel Electronics, 2015
#

import platform
import os
import stat

def which(pgm):
    path=os.getenv('PATH')
    for p in path.split(os.path.pathsep):
        p=os.path.join(p,pgm)
        if os.path.exists(p) and os.access(p,os.X_OK):
            return p

def isSystem(system):
    return platform.system() == system

package_name="SEP (SVN Externals Package)"
svn_exec="svn"
svn_actual="svnactual"
svn_script="svn"
svn_jar="svn.jar"
if isSystem('Windows'):
    svn_exec+=".exe"
    svn_actual+=".exe"
    svn_script+=".bat"

if isSystem('Windows') | isSystem('Linux'):
    svn_full_path=which(svn_exec)
    svn_actual_full_path=which(svn_actual)
    
    if svn_actual_full_path is not None:
        path, file = os.path.split(svn_actual_full_path)
        
        os.remove(path + "/" + svn_script)
        print "\nRemoved Svn Script."
        
        os.remove(path + "/" + svn_jar)
        print "Removed executable Jar file."
        
        os.rename(svn_actual_full_path, path + "/" + svn_exec)
        print "Renamed actual SVN executable."
        
        if isSystem('Linux'):
            st = os.stat(svn_full_path)
            os.chmod(svn_full_path, st.st_mode | stat.S_IEXEC | stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)
        
        print "\n" + package_name + " is UNINSTALLED...\n"
        
    elif svn_full_path is not None:
        
        print "\n" + package_name + " was already UNINSTALLED...\n"
    else:
        print "\nSVN is not installed in this computer.\nPlease, install SVN Command Line Interface...\n\n"

else:
    print 'Unknown Platform'

    
