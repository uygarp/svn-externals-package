/*
 * Name: GLOBAL.java
 * Author: Uygar Poyraz (uygarp)
 * Description: GLOBAL interface implementation
 *
 */

public interface GLOBAL
{
    boolean DEBUG = false;

    int EXIT_SUCCESS = 0;
    int EXIT_FAILURE = 1;

    String[] UrlHeaders = {"file:///", "http://", "https://", "svn://", "svn+ssh://"};

    String name = "SEP (SVN Externals Package)";
    String owner = "Vestel Electronics, R&D Software Group";
    String description = "This package gives the ability of \"including external path logs to the revision log list\"\n" + "to SVN Command Line Interface.\n" + "It works as an extention for SVN, and does not introduce any new command or parameter.";
    String version = "V0.1";
    String author = "Uygar Poyraz [uygarp]";
    String mail1 = "uygar.poyraz@vestel.com.tr";
    String mail2 = "poyraz.uygar@gmail.com";

    String verString = "------------------------------------------------------------------------------\n" + "\n" + name + ":" + "\n" + description + "\n" + "\n" + owner + "\n" + "\nVersion: " + version + "\nAuthor : " + author + "\nE-mail : " + mail1 + ", " + mail2 + "\n";
}
