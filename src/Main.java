/*
 * Name: Main.java
 * Author: Uygar Poyraz (uygarp)
 * Description: Main class implementation
 *
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class Main
{
    private static SvnCmd svn = SvnCmd.getInstance();
    private static ProcessRunner svnProc = new ProcessRunner();

    public static void main(String[] args) throws IOException, InterruptedException, ParserConfigurationException, SAXException, ParseException
    {
        if(GLOBAL.DEBUG)
        {
            // String path =
            // "http://svn/repos/SingleChipTV/branches/projects/Genki";
            String path = "http://svn/repos/SingleChipTV/branches/projects/Cyberman";
            // String path =
            // "http://svn/repos/SingleChipTV/branches/projects/Artwin";
            // String path =
            // "http://svn/repos/SingleChipTV/branches/projects/Toros_prod_phase1";
            // String path = "http://svn/repos/SingleChipTV/branches/Phoenix";
            // String path = "D:\\Workspace\\Phoenix_OBS";
            // String path = "D:\\Workspace\\Genki\\app_dtv";
            // String path = "B:\\Genki";

            String[] tempArgs = {"svnActual", "--version"};
            // String[] tempArgs = {"svnpure", "--version", "--quiet"};
            // String[] tempArgs = {"svnpure", "info",
            // "D:\\Workspace\\Phoenix_OBS", "--xml"};
            // String[] tempArgs = {"svnpure", "--version"};
            // String [] tempArgs =
            // {"C:\\Program Files\\TortoiseSVN\\bin\\svnold"};
            // String[] tempArgs = {"svnActual", "log", "-r119300:119200",
            // path};
            // String[] tempArgs = {"svnActual", "log", "-l3", path};
            args = tempArgs;
        }

        if(args == null || args.length < 1)
        {
            println("Error in Command Args... [args is null or empty]");
            System.exit(GLOBAL.EXIT_FAILURE);
        }

        svn.setCmd(args[0]);
        List<String> Args = getSvnArgs(args);

        SvnArgParser svnArgParser = new SvnArgParser();
        if(svnArgParser.hasVersionNoQuiet(Args))
        {
            printVersionAndExit(Args);
        }

        if(!svnArgParser.hasLogArg(Args))
        {
            println("Error in Command Args... [svnArgParser.isLogCommandArgs(Args)]");
            RunSvnCommandAndExit(Args);
        }

        SvnCommandPath userPath = svnArgParser.getPath(Args);
        if(userPath == null || userPath.getPath().equals(""))
        {
            println("Error in User Path... [svnArgParser.getPath(Args)]");
            RunSvnCommandAndExit(Args);
        }

        SvnInfo info = getSvnInfo(userPath);
        if(info == null)
        {
            println("Error in Svn Info... [getSvnInfo(userPath)]");
            RunSvnCommandAndExit(Args);
        }

        userPath.setSvnInfo(info);

        String testCommand = getTestCommand(Args, userPath);
        if(!svnProc.isLogCommandValid(testCommand))
        {
            println("Error in Command... [svnProc.isLogCommandValid(commandString)]");
            RunSvnCommandAndExit(Args);
        }

        List<SvnExternalPath> externalPathList = getExternalPaths(userPath.getSvnInfo().getURL());
        if(externalPathList.size() == 0)
        {
            println("No External paths found, run Pure SVN command... [getExternalPaths(targetPath.getPath())]");
            RunSvnCommandAndExit(Args);
        }

        // This is the user link itself. We added this as an external link.
        String selfPath = userPath.getSvnInfo().getURL().replaceAll(userPath.getSvnInfo().getRepoRoot(), "");
        String[] selfFolders = selfPath.split("\\/");
        externalPathList.add(new SvnExternalPath(selfPath, selfFolders[selfFolders.length - 1]));

        SvnCommandPath targetPath = normalizeExternalPaths(userPath, externalPathList);// URL
        if(targetPath == null || targetPath.getPath().equals(""))
        {
            println("Error occurred while normalizing external paths... [normalizeExternalPaths(userPath, externalPathList);]");
            RunSvnCommandAndExit(Args);
        }

        List<String> modifiedArgs = removeUserPathsFromArgs(Args, userPath);
        modifiedArgs.add(" " + targetPath.getPath());
        for(int i = 0; i < targetPath.getChildDirectories().size(); i++)
        {
            modifiedArgs.add(" " + targetPath.getChildDirectories().get(i));
        }

        RunSvnCommandAndExit(modifiedArgs);
    }

    private static List<SvnExternalPath> getExternalPaths(String path) throws IOException, ParserConfigurationException, SAXException
    {
        String rawXML = svnProc.runCommand(svn.getCmd() + "propget svn:externals --xml " + path, false);
        if(!errorCheckOfCommandOutput(rawXML))
        {
            return (new XmlParser()).getExternalPathsFromXML(rawXML);
        }
        return null;
    }

    private static SvnInfo getSvnInfo(SvnCommandPath path) throws IOException, ParserConfigurationException, SAXException, ParseException
    {
        String rawXML = svnProc.runCommand(svn.getCmd() + "info --xml " + path.getPath(), false);
        if(!errorCheckOfCommandOutput(rawXML))
        {
            return (new XmlParser()).getInfoFromXML(rawXML);
        }
        return null;
    }

    private static boolean errorCheckOfCommandOutput(String output)
    {
        BufferedReader outputReader = new BufferedReader(new StringReader(output));
        String outputLine = "";
        try
        {
            outputLine = outputReader.readLine();

            while(outputLine != null)
            {
                if(outputLine.startsWith("svn:"))
                {
                    return true;
                }
                outputLine = outputReader.readLine();
            }
        }
        catch(IOException e)
        {
            return true;
        }

        return false;
    }

    private static String getTestCommand(List<String> Args, SvnCommandPath path)
    {
        String commandString = svn.getCmd();
        boolean pathAdded = false;

        for(int i = 0; i < Args.size(); i++)
        {
            if(!pathAdded && Args.get(i).equals(path.getPath()))
            {
                commandString += path.getSvnInfo().getURL() + " ";
                pathAdded = true;
            }
            else
            {
                if(pathAdded)
                {
                    int index = path.getChildDirectories().indexOf(Args.get(i));
                    if(index == -1)
                    {
                        commandString += Args.get(i) + " ";
                    }
                }
                else
                {
                    commandString += Args.get(i) + " ";
                }
            }
        }

        return commandString;
    }

    static SvnCommandPath normalizeExternalPaths(SvnCommandPath commandPath, List<SvnExternalPath> externalPathList)
    {
        SvnCommandPath path = null;

        if(externalPathList != null || commandPath != null)
        {
            path = new SvnCommandPath();
            List<SvnExternalPath> extPathListToGet = new ArrayList<SvnExternalPath>();

            if(commandPath.getChildDirectories().size() > 0)
            {
                for(int i = 0; i < commandPath.getChildDirectories().size(); i++)
                {
                    boolean found = false;
                    for(int j = 0; j < externalPathList.size(); j++)
                    {
                        String compare1 = commandPath.getChildDirectories().get(i);
                        String compare2 = externalPathList.get(j).getFolder();
                        if(compare1.equals(compare2))
                        {
                            extPathListToGet.add(externalPathList.get(j));
                            found = true;
                            break;
                        }
                    }

                    if(!found)
                    {
                        extPathListToGet = null;
                        break;
                    }
                }
            }
            else
            {
                extPathListToGet = externalPathList;
            }

            if(extPathListToGet != null && extPathListToGet.size() > 0)
            {
                String correctCommonFolder = "";
                String checkCommonFolder = "";
                String[] folderPartArray = extPathListToGet.get(0).getLink().split("\\/");

                FOLDERPARTCHECK:
                for(int i = 0; i < folderPartArray.length; i++)
                {
                    checkCommonFolder += folderPartArray[i] + "/";

                    for(int j = 0; j < extPathListToGet.size(); j++)
                    {
                        if(!extPathListToGet.get(j).getLink().startsWith(checkCommonFolder))
                        {
                            break FOLDERPARTCHECK;
                        }
                    }

                    correctCommonFolder = checkCommonFolder;
                }

                path.setPath(commandPath.getSvnInfo().getRepoRoot() + correctCommonFolder);

                for(int i = 0; i < extPathListToGet.size(); i++)
                {
                    String newFolder = extPathListToGet.get(i).getLink().substring(correctCommonFolder.length());
                    path.addChildDirectory(newFolder);
                }
            }
        }

        return path;
    }

    static List<String> removeUserPathsFromArgs(List<String> args, SvnCommandPath path)
    {
        List<String> newArgs = new ArrayList<String>();

        if(!path.getPath().equals(""))
        {
            for(int i = 0; i < args.size(); i++)
            {
                boolean isPathMember = false;

                if(args.get(i).equals(path.getPath()))
                {
                    isPathMember = true;
                }
                else
                {
                    for(int j = 0; j < path.getChildDirectories().size(); j++)
                    {
                        if(args.get(i).equals(path.getChildDirectories().get(j)))
                        {
                            isPathMember = true;
                            break;
                        }
                    }
                }

                if(!isPathMember)
                {
                    newArgs.add(args.get(i));
                }
            }
        }

        return newArgs;
    }

    static SvnCommandPath convertURLCommandPath(SvnCommandPath path) throws IOException, ParserConfigurationException, SAXException, ParseException
    {
        SvnCommandPath urlPath = null;
        if(path != null)
        {
            urlPath = new SvnCommandPath();
            String pathString = "";
            if(path.getPath().equals(""))
            {
                pathString = System.getProperty("user.dir");
                println("Target Directory = " + System.getProperty("user.dir"));
            }
            else if(path.isLocalDirectory())
            {
                pathString = path.getSvnInfo().getURL();
            }
            else
            {
                pathString = path.getPath();
            }

            urlPath.setPath(pathString);

            for(int i = 0; i < path.getChildDirectories().size(); i++)
            {
                urlPath.addChildDirectory(path.getChildDirectories().get(i));
            }

            urlPath.setSvnInfo(path.getSvnInfo());
        }

        return urlPath;
    }

    private static void RunSvnCommandAndExit(List<String> Args) throws IOException
    {
        System.exit(svnProc.runSvnCommand(Args));
    }

    private static List<String> getSvnArgs(String[] argList)
    {
        List<String> Args = null;

        if(argList != null)
        {
            Args = new ArrayList<String>();
            for(int i = 1; i < argList.length; i++)
            {
                println("argList[" + i + "]=" + argList[i]);
                Args.add(argList[i]);
            }
        }

        return Args;
    }

    private static void printVersionAndExit(List<String> Args) throws IOException
    {
        int status = svnProc.runSvnCommand(Args);

        System.out.println(GLOBAL.verString);

        System.exit(status);
    }

    private static void println(String str)
    {
        if(GLOBAL.DEBUG)
        {
            System.out.println(str);
        }
    }
}
