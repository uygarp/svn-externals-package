/*
 * Name: ProcessRunner.java
 * Author: Uygar Poyraz (uygarp)
 * Description: ProcessRunner class implementation
 *
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.List;

public class ProcessRunner
{
    private static Process process;

    private static BufferedReader outputReader = null;
    private static BufferedReader errorReader = null;
    private static PrintStream printStream = null;
    private static SvnCmd svn = SvnCmd.getInstance();

    void setCommand(String command) throws IOException
    {
        process = Runtime.getRuntime().exec(command);

        outputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));

        printStream = new PrintStream(System.out, true, "ibm857");

    }

    String readLine() throws IOException
    {
        String outputLine = outputReader.readLine();
        if(outputLine == null)
        {
            outputLine = errorReader.readLine();

            if(outputLine == null)
            {
                try
                {
                    process.waitFor();
                }
                catch(InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        }

        return outputLine;
    }

    public String runCommand(String command, boolean enableOutput) throws IOException
    {
        setCommand(command);
        String line = readLine();
        StringBuilder commandOut = new StringBuilder();

        while(line != null)
        {
            commandOut.append(line + "\n");
            if(enableOutput)
            {
                printlnConsole(line);
            }
            line = readLine();
        }

        return commandOut.toString();
    }

    public int runSvnCommand(List<String> Args) throws IOException
    {
        String command = svn.getCmd();

        boolean commandExist = false;

        if(Args != null)
        {
            for(int i = 0; i < Args.size(); i++)
            {
                command += " " + Args.get(i);
            }

            commandExist = command != null && !command.equals("");
        }

        if(commandExist)
        {
            runCommand(command, true);
            return process.exitValue();
        }
        else
        {
            return GLOBAL.EXIT_FAILURE;
        }
    }

    public boolean isLogCommandValid(String command) throws IOException
    {
        setCommand(command + " -l1");
        String line = readLine();

        while(line != null)
        {
            if(hasSvnError(line))
            {
                return false;
            }

            line = readLine();
        }

        return true;
    }

    public boolean hasSvnError(String line)
    {
        if(line != null)
        {
            if(!line.startsWith("svn:"))
            {
                return false;
            }
        }

        return true;
    }

    private static void printlnConsole(String text)
    {
        printStream.println(text);
    }
}
