/*
 * Name: XmlParser.java
 * Author: Uygar Poyraz (uygarp)
 * Description: XmlParser class implementation
 *
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XmlParser
{
    public NodeList parser(String rawXML) throws ParserConfigurationException, SAXException, IOException
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        StringReader sr = new StringReader(rawXML);
        InputSource is = new InputSource(sr);
        // is.setEncoding("UTF-8");
        Document document = (Document) builder.parse(is);

        NodeList nodeList = ((Node) document.getDocumentElement()).getChildNodes();

        return nodeList;
    }

    public List<SvnExternalPath> getExternalPathsFromXML(String rawXML) throws IOException, ParserConfigurationException, SAXException
    {
        List<SvnExternalPath> exPathList = new ArrayList<SvnExternalPath>();

        NodeList nodeList = parser(rawXML);

        Node target = getNode("target", nodeList);
        if(target != null)
        {
            Node property = getNode("property", target.getChildNodes());
            if(property != null)
            {
                if(getNodeAttr("name", property).equals("svn:externals"))
                {
                    BufferedReader bufReader = new BufferedReader(new StringReader(getNodeValue(property)));
                    String line = null;
                    while((line = bufReader.readLine()) != null)
                    {
                        String link = line.split("\\ ", 2)[0].substring(1);
                        String folder = line.split("\\ ", 2)[1].replaceAll(" ", "");
                        exPathList.add(new SvnExternalPath(link, folder));
                    }
                }
            }
        }

        return exPathList;
    }

    public String getURLPathFromXML(String rawXML) throws IOException, ParserConfigurationException, SAXException
    {
        String url = "";

        Node entry = getNode("entry", parser(rawXML));
        if(entry != null)
        {
            url = getNodeValue("url", entry.getChildNodes());
        }

        return url;
    }

    public SvnInfo getInfoFromXML(String rawXML) throws IOException, ParserConfigurationException, SAXException, ParseException
    {
        SvnInfo svnInfo = null;

        Node entry = getNode("entry", parser(rawXML));

        if(entry != null)
        {
            svnInfo = new SvnInfo();

            svnInfo.setPath(getNodeAttr("path", entry));
            svnInfo.setRevision(Long.parseLong(getNodeAttr("revision", entry)));
            svnInfo.setKind(getNodeAttr("kind", entry));

            svnInfo.setURL(getNodeValue("url", entry.getChildNodes()));

            Node repo = getNode("repository", entry.getChildNodes());

            if(repo != null)
            {
                svnInfo.setRepoRoot(getNodeValue("root", repo.getChildNodes()));
                svnInfo.setRepoUUID(getNodeValue("uuid", repo.getChildNodes()));
            }

            Node commit = getNode("commit", entry.getChildNodes());

            if(commit != null)
            {
                svnInfo.setCommitRevision(Long.parseLong(getNodeAttr("revision", commit)));
                svnInfo.setCommitAuthor(getNodeValue("author", commit.getChildNodes()));

                String dateString = getNodeValue("date", commit.getChildNodes());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                svnInfo.setCommitDate(sdf.parse(dateString));
            }

        }

        return svnInfo;
    }

    protected Node getNode(String tagName, NodeList nodes)
    {
        for(int x = 0; x < nodes.getLength(); x++)
        {
            Node node = nodes.item(x);
            if(node.getNodeName().equalsIgnoreCase(tagName))
            {
                return node;
            }
        }

        return null;
    }

    protected String getNodeValue(Node node)
    {
        NodeList childNodes = node.getChildNodes();
        for(int x = 0; x < childNodes.getLength(); x++)
        {
            Node data = childNodes.item(x);
            if(data.getNodeType() == Node.TEXT_NODE)
            {
                return data.getNodeValue();
            }
        }
        return "";
    }

    protected String getNodeValue(String tagName, NodeList nodes)
    {
        for(int x = 0; x < nodes.getLength(); x++)
        {
            Node node = nodes.item(x);
            if(node.getNodeName().equalsIgnoreCase(tagName))
            {
                NodeList childNodes = node.getChildNodes();
                for(int y = 0; y < childNodes.getLength(); y++)
                {
                    Node data = childNodes.item(y);
                    if(data.getNodeType() == Node.TEXT_NODE)
                    {
                        return data.getNodeValue();
                    }
                }
            }
        }
        return "";
    }

    protected String getNodeAttr(String attrName, Node node)
    {
        NamedNodeMap attrs = node.getAttributes();
        for(int y = 0; y < attrs.getLength(); y++)
        {
            Node attr = attrs.item(y);
            if(attr.getNodeName().equalsIgnoreCase(attrName))
            {
                return attr.getNodeValue();
            }
        }
        return "";
    }

    protected String getNodeAttr(String tagName, String attrName, NodeList nodes)
    {
        for(int x = 0; x < nodes.getLength(); x++)
        {
            Node node = nodes.item(x);
            if(node.getNodeName().equalsIgnoreCase(tagName))
            {
                NodeList childNodes = node.getChildNodes();
                for(int y = 0; y < childNodes.getLength(); y++)
                {
                    Node data = childNodes.item(y);
                    if(data.getNodeType() == Node.ATTRIBUTE_NODE)
                    {
                        if(data.getNodeName().equalsIgnoreCase(attrName))
                        {
                            return data.getNodeValue();
                        }
                    }
                }
            }
        }

        return "";
    }
}
